import mz.vodacom.people.Constants;
import mz.vodacom.people.Utils;
import mz.vodacom.people.persistence.JpaConfiguration;
import mz.vodacom.people.persistence.Person;
import mz.vodacom.people.persistence.PersonJpaRepository;
import mz.vodacom.people.persistence.UUIDGenerator;
import mz.vodacom.people.rest.PeopleRestApp;
import mz.vodacom.people.rest.RestException;
import mz.vodacom.people.rest.resources.PersonDto;
import mz.vodacom.people.rest.resources.PersonRestResource;
import mz.vodacom.people.service.PeopleService;
import mz.vodacom.people.service.ServiceException;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.File;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class ArquillianExampleTest {

    @Inject
    private PeopleService peopleService;

    @Deployment
    public static WebArchive createDeployment() {
        File[] libs = Maven.resolver()
                .loadPomFromFile("pom.xml").importRuntimeDependencies().resolve().withTransitivity()
                .asFile();

        WebArchive jar =  ShrinkWrap.create(WebArchive.class,"test.war")
                .addPackage("mz.vodacom.people")
                .addPackage("mz.vodacom.people.persistence")
                .addPackage("mz.vodacom.people.rest")
                .addPackage("mz.vodacom.people.rest.resources")
                .addPackage("mz.vodacom.people.service")
                .addAsLibraries(libs)
                //.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/resources/META-INF/beans.xml"), "beans.xml")
                .add(new FileAsset(new File("src/test/resources/persistence-standalone.xml")),"/WEB-INF/classes/META-INF/persistence.xml")
                .setWebXML("web.xml");

        System.out.println(jar.toString(true));
        return jar;
    }


    @Test
    public void person_creation_should_succeed() {

        List<Person> people = peopleService.everyone();
        Assert.assertEquals(0,people.size());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,1993);
        calendar.set(Calendar.MONTH,Calendar.SEPTEMBER);
        calendar.set(Calendar.DATE,15);

        String personId = peopleService.create("Mario","Junior",
                "francisco.junior.mario@gmail.com",
                calendar.getTime());
        people = peopleService.everyone();
        assertEquals(1,people.size());

    }

    @Test(expected = IllegalArgumentException.class)
    public void person_must_not_be_born_in_the_future(){

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,2025);
        calendar.set(Calendar.MONTH,Calendar.SEPTEMBER);
        calendar.set(Calendar.DATE,15);

        String personId = peopleService.create("Mario","Junior",
                "francisco.junior.mario@gmail.com",
                calendar.getTime());

    }

    @Test(expected = IllegalArgumentException.class)
    public void person_email_must_be_properly_formatted(){

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,1985);
        calendar.set(Calendar.MONTH,Calendar.SEPTEMBER);
        calendar.set(Calendar.DATE,15);

        String personId = peopleService.create("Mario","Junior",
                "francisco.junior.mario:gmail.com",
                calendar.getTime());

    }


}
