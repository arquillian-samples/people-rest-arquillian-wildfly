package mz.vodacom.people;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static Date parseDate(String date){
        try {
            SimpleDateFormat format = new SimpleDateFormat(Constants.SIMPLE_DATE_FORMAT);
            return format.parse(date);
        }catch (ParseException ex){
            throw new IllegalArgumentException("invalid date: "+date);
        }
    }


    public static String format(Date date){

        return new SimpleDateFormat(Constants.SIMPLE_DATE_FORMAT)
                .format(date);

    }


}
