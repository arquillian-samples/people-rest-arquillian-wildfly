package mz.vodacom.people.persistence;

import javax.persistence.PrePersist;
import java.util.UUID;

public class UUIDGenerator {

    @PrePersist
    public void setId(Person person){
        if(person.getId()==null)
            person.setId(UUID.randomUUID().toString());
    }

}